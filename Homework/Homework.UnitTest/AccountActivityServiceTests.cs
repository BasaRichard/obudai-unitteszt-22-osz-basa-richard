﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using Moq;
using Homework.ThirdParty;

namespace Homework.UnitTest
{
    [TestFixture]
    class AccountActivityServiceTests
    {
        private AccountActivityService accountActivityService;
        Mock<IAction> mockIActionn = new Mock<IAction>();
        [OneTimeSetUp]
        public void SetUp()
        {
            FakeAccountRepositroy fakeAccountRepositroy = new FakeAccountRepositroy();
            fakeAccountRepositroy.Add(new Account(1));
            fakeAccountRepositroy.Add(new Account(2));
            fakeAccountRepositroy.Add(new Account(3));
            fakeAccountRepositroy.Add(new Account(4));

            mockIActionn.Setup(x => x.Execute()).Returns(true);
            IAction action = mockIActionn.Object;
            foreach (var account in fakeAccountRepositroy.GetAll())
            {
                //tudom hogy nem a legg jobb de igy legalább a tesztek lényegre török 
                account.Activate();
                account.Register();
                if (account.Id == 2)
                {
                    account.TakeAction(action);
                }
                if (account.Id == 3)
                {
                    for (int i = 0; i < 30; i++)
                    {
                        account.TakeAction(action);
                    }
                }
                if (account.Id == 4)
                {
                    for (int i = 0; i < 50; i++)
                    {
                        account.TakeAction(action);
                    }
                }
            }
            //mockRepositroy = new Mock<IAccountRepository>();
            accountActivityService = new AccountActivityService(fakeAccountRepositroy);
            //accountActivityService = new AccountActivityService(mockRepo.Object);

        }

        [Test]
        [Category("Basic GetActivity")]
        public void NullReferenc_GetActivity_ThrowsAccountNotExistsException()
        {
           
            Assert.That(() => accountActivityService.GetActivity(5), Throws.TypeOf<AccountNotExistsException>());
        }

        [Test]
        [Category("Basic GetActivity")]
        public void SetActivityLevelNone_GetActivity_SetSuccesfully()
        {
            Assert.That(accountActivityService.GetActivity(1), Is.EqualTo(ActivityLevel.None));
        }

        [Test]
        [Category("Basic GetActivity")]
        public void SetActivityLevelLow_GetActivity_SetSuccesfully()
        {
            Assert.That(accountActivityService.GetActivity(2), Is.EqualTo(ActivityLevel.Low));
        }

        [Test]
        [Category("Basic GetActivity")]
        public void SetActivityLevelMedium_GetActivity_SetSuccesfully()
        {
            Assert.That(accountActivityService.GetActivity(3), Is.EqualTo(ActivityLevel.Medium));
        }

        [Test]
        [Category("Basic GetActivity")]
        public void SetActivityLevelHigh_GetActivity_SetSuccesfully()
        {
            Assert.That(accountActivityService.GetActivity(4), Is.EqualTo(ActivityLevel.High));
        }

        [Test]
        [Category("Basic GetAmountForActivity")]
        public void GetActivityLevelCount_GetAmountForActivity_SuccesfullyCount()
        {
            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.None), Is.EqualTo(1));
        }

        [Test]
        [Category("Basic GetAmountForActivity")]
        public void GetActivityLevelLowCount_GetAmountForActivity_SuccesfullyCount()
        {
            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.Low), Is.EqualTo(1));
        }

        [Test]
        [Category("Basic GetAmountForActivity")]
        public void GetActivityLevelMediumCount_GetAmountForActivity_SuccesfullyCount()
        {
            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.Medium), Is.EqualTo(1));
        }

        [Test]
        [Category("Basic GetAmountForActivity")]
        public void GetActivityLevelHighCount_GetAmountForActivity_SuccesfullyCount()
        {
            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.High), Is.EqualTo(1));
        }

        [Test]
        [Category("Complex GetAmountForActivity")]
        public void ShouldCalculateGetAmountForAtivity_GetAmountForActivity_a()
        {
            FakeAccountRepositroy fakeAccountRepositroy = new FakeAccountRepositroy();
            fakeAccountRepositroy.Add(new Account(1));
            fakeAccountRepositroy.Add(new Account(2));
            fakeAccountRepositroy.Add(new Account(3));
            fakeAccountRepositroy.Add(new Account(4));
            fakeAccountRepositroy.Add(new Account(5));
            fakeAccountRepositroy.Add(new Account(6));
            mockIActionn.Setup(x => x.Execute()).Returns(true);
            IAction action = mockIActionn.Object;
            foreach (var account in fakeAccountRepositroy.GetAll())
            {
                account.Activate();
                account.Register();
                account.TakeAction(action);
            }
            accountActivityService = new AccountActivityService(fakeAccountRepositroy);
            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.Low), Is.EqualTo(6));
        }


    }
}
