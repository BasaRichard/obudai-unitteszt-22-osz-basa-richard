﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTest
{
    class FakeAccountRepositroy : IAccountRepository
    {
        private readonly List<Account> accountRepository = new List<Account>();

        public bool Add(Account account)
        {
            accountRepository.Add(account);
            return true;
        }

        public bool Exists(int accountId)
        {
            var toFind = accountRepository.FirstOrDefault(x => x.Id == accountId);
            if (toFind != null)
            {
                return true;
            }
            return false;
        }

        public Account Get(int accountId)
        {
            return accountRepository.FirstOrDefault(x => x.Id == accountId);
        }

        public IEnumerable<Account> GetAll()
        {
            return accountRepository.ToList();
        }

        public bool Remove(int accountId)
        {
            var toDelete = accountRepository.FirstOrDefault(x => x.Id == accountId);
            if (toDelete != null)
            {
                accountRepository.Remove(toDelete);
                return true;
            }
            return false;
        }
    }
}
