﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using Moq;
using Homework.ThirdParty;

namespace Homework.UnitTest
{
    [TestFixture]
    public class AccountTests
    {
        private Account tempAccount;
        private Mock<IAction> mockIAction;
        
        [SetUp]
        public void SetUP()
        {
            tempAccount = new Account(1);
            mockIAction = new Mock<IAction>();
        }

        [Test]
        [Category("Bassic Acount Tests")]
        public void ActivateRegister_Register_SuccesfullyRegistered()
        {
            
            //act
            tempAccount.Register();
            // assert
            Assert.That(tempAccount.IsRegistered, Is.True);
        }

        [Test]
        [Category("Bassic Acount Tests")]
        public void DefaultResoult_Register_FalseRegistered()
        {
            // assert
            Assert.That(tempAccount.IsRegistered, Is.False);
        }

        [Test]
        [Category("Bassic Acount Tests")]
        public void ActivateActivate_Activate_SuccesfullyActivated()
        {
            //act
            tempAccount.Activate();
            // assert
            Assert.That(tempAccount.IsConfirmed, Is.True);
        }
        [Test]
        [Category("Bassic Acount Tests")]
        public void DefaultResoult_Activate_FalseActivated()
        {
            // assert
            Assert.That(tempAccount.IsConfirmed, Is.False);
        }

        [Test]
        [Category("Complex  TakeAction")]
        public void FailPerformAction_TakeAction_ThrowInactiveUserException()
        {
            Assert.That(() => tempAccount.TakeAction(mockIAction.Object), Throws.TypeOf<InactiveUserException>());
            mockIAction.Verify(x => x.Execute(), Times.Never);
            mockIAction.VerifyNoOtherCalls();
        }

        [Test]
        [Category("Complex TakeAction")]
        public void IncreaseActionsSuccessfullyPerformed_TakeAction_SuccesfullyIncrease()
        {
            tempAccount.Register();
            tempAccount.Activate();
            mockIAction.Setup(x => x.Execute()).Returns(true);

            IAction action = mockIAction.Object;
            int defaultNumber = tempAccount.ActionsSuccessfullyPerformed;
            tempAccount.TakeAction(action);

            Assert.That(defaultNumber, Is.Not.EqualTo(tempAccount.ActionsSuccessfullyPerformed));
            mockIAction.Verify(x => x.Execute(), Times.Once);
        }
        [Test]
        [Category("Complex TakeAction")]
        public void IncreaseActionsSuccessfullyPerforme_TakeAction_SuccesfullyIncrease()
        {
            tempAccount.Register();
            tempAccount.Activate();
            mockIAction.Setup(x => x.Execute()).Returns(true);

            IAction action = mockIAction.Object;
            int defaultNumber = tempAccount.ActionsSuccessfullyPerformed;
            tempAccount.TakeAction(action);
            tempAccount.TakeAction(action);
            tempAccount.TakeAction(action);

            Assert.That(defaultNumber, Is.Not.EqualTo(tempAccount.ActionsSuccessfullyPerformed));
            mockIAction.Verify(x => x.Execute(),Times.Exactly(3));
        }
    }
}
